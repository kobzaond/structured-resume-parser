from os import times
from datetime import datetime
from difflib import get_close_matches
import re
from sentence_transformers import SentenceTransformer, util
import torch
from enum import Enum
from fuzzywuzzy import fuzz
import spacy
from spacy.matcher import Matcher
from transformers import AutoModelForQuestionAnswering, AutoTokenizer, pipeline
from ibc_utils import delete_weird_chars, sutime_parser, filter_duration_dates, QA_Model, extract_name, has_numbers, delete_weird_chars
from collections import OrderedDict
from dateutil.parser import parse

# Load SpaCy
nlp = spacy.load("en_core_web_sm")
matcher = Matcher(nlp.vocab)


class TypeOfLine(Enum):
    Simple = 1
    KeyValue = 2
    Complex = 3
    LongSimple = 4


class InformationExtractor:

    def __init__(self, path) -> None:
        self.entities_of_interest = ['experience', 'education', 'certification', 'interests', 'languages', 'training', 'competitions',
                            'projects', 'skill', 'affiliations', 'accomplishments', 'publication', 'references', 'traits', 'personal']
        self.entities_of_interest_synsets = {
            'experience': ['experience', 'employment', 'professional', 'career recital', 'project work'],
            'education': ['education', 'academic', 'academics', 'scholastic'],
            'certification': ['certification'],
            'interests': ['interest', 'hobby', 'hobbies'],
            'languages': ['language'],
            'projects': ['project'],
            'skill': ['skill', 'key strength', 'strength', 'strengths'],
            'summary': ['summary'],
            'affiliations': ['affiliations'],
            'accomplishments': ['accomplishments', 'achievements', 'achiements', 'achievments'],
            'publication':['publication'],
            'references': ['references'],
            'training': ['training'],
            'competitions': ['competitions'],
            'traits': ['personality traits'],
            'personal': ['personal information', 'personal dossier', 'personal']
        }
        self.strictly_non_date = ['languages', 'skill', 'summary', 'interests']
        self.long_sec_items = ['references', 'summary', 'accomplishments', 'affiliations']
        self.date_based_entities = ['experience', 'education', 'publication', 'accomplishments']

        self.personal_information = ['name', 'phone number', 'e-mail', 'address', 'date of birth',
                            'marital status', 'nationality', 'web']

        self.entities_must = ['skills', 'experience', 'name', 'phone', 'e-mail', 'address', 'date of birth', 'education']

        self.specific_questions = {
            'education': ['What faculty did he study?', 'What school or university did he study?', 'What was his major /course?'],
            'experience': ['For what company did he work?', 'What is the name / designation of his job position?']
        }
        self.spec_tags = {
            'education': ['faculty', 'institution', 'field_of_study'],
            'experience': ['firm', 'job']
        }
        self.lang_levels = ['A0', 'A1', 'A2', 'B1', 'B2', 'C1', 'C2']
        self.forbidden_words_in_section = ['internship', 'education', 'references', 'summary', 'accomplishment', 'address']
        
        f =open(path, 'r+')
        self.lines = list(map(lambda x: x.replace("\n", ""), list(f.readlines())))
        self.lines = list(filter(lambda x: True if len(x)>0 else False, self.lines))
        f.close()
        self.text = " | ".join(self.lines)
        f = open('languages.txt', 'r+')
        self.langs = list(map(lambda x: x.replace("\n", "").lower(), list(f.readlines())))
        f.close()

        # self.model = SentenceTransformer('average_word_embeddings_glove.6B.300d')
 
        self.blanks_ratio = self.get_blank_line_between_text_lines_rate()
        self.basic_labels = []
        model_name = "deepset/bert-large-uncased-whole-word-masking-squad2"
        nlp = pipeline('question-answering', model=model_name, tokenizer=model_name)
        self.model = QA_Model(nlp)
        self.mail = None
        # self.sutime = SUTime(mark_time_ranges=True, include_range=True)


    def get_blank_line_between_text_lines_rate(self):
        count = 0
        c_t = 0
        for i in range(1,len(self.lines)-1):
            if len(self.lines[i]) == 0 and len(self.lines[i-1])>1 and len(self.lines[i+1])>1:
                count += 1
            if len(self.lines[i])>1:
                c_t +=1 
        return count/c_t

    def compute_entity_ratio(self, entity, text):
        for item in self.entities_of_interest_synsets[entity]:
            ratio = fuzz.partial_ratio(item, text)
            if ratio>91:
                return item, ratio
        return None, 0

    def contains_fw(self, text):
        for fw in self.forbidden_words_in_section:
            if fw in text.lower():
                return True
        return False

    def getEmail(self, inputString, debug=False): 
        '''
        Given an input string, returns possible matches for emails. Uses regular expression based matching.
        Needs an input string, a dictionary where values are being stored, and an optional parameter for debugging.
        Modules required: clock from time, code.
        '''

        email = None
        try:
            pattern = re.compile(r'\S*@\S*')
            matches = pattern.findall(inputString) # Gets all email addresses as a list
            email = matches
        except Exception as e:
            print(e)

        return email

    def getPhone(self, inputString):
        '''
        Given an input string, returns possible matches for phone numbers. Uses regular expression based matching.
        Needs an input string, a dictionary where values are being stored, and an optional parameter for debugging.
        Modules required: clock from time, code.
        '''

        number = None
        try:
            pattern = re.compile(r'([+(]?\d+[)\-]?[ \t\r\f\v]*[(]?\d{2,}[()\-]?[ \t\r\f\v]*\d{2,}[()\-]?[ \t\r\f\v]*\d*[ \t\r\f\v]*\d*[ \t\r\f\v]*)')
            match = pattern.findall(inputString)
            match = [re.sub(r'[,.]', '', el) for el in match if len(re.sub(r'[()\-.,\s+]', '', el))>6]
            match = [re.sub(r'\D$', '', el).strip() for el in match]
            match = [el for el in match if len(re.sub(r'\D','',el)) <= 15]
            try:
                for el in list(match):
                    if len(el.split('-')) > 3: continue # Year format YYYY-MM-DD
                    for x in el.split("-"):
                        try:
                            if x.strip()[-4:].isdigit():
                                if int(x.strip()[-4:]) in range(1900, 2100):
                                    match.remove(el)
                        except:
                            pass
            except:
                pass
            number = match
        except:
            pass

        return number

    def filter_out_empty_linedicts(self):
        new_lines = []
        for i in range(len(self.lines)):
            if i<len(self.lines)-1:
                if (self.lines[i][-1] == ':' or self.lines[i][-1] == '-') and (self.lines[i+1][-1] == ':' or self.lines[i+1][-1] == '-'):
                    continue
            new_lines.append(self.lines[i])
        self.lines = new_lines
        self.text = " | ".join(new_lines)

    def get_date_of_birth(self):
        times = sutime_parser(self.text[0:int(len(self.text)*0.25)])
        if len(times)==0:
            return None
        time = times[0]
        start = time['start']-20
        if 'birth' in self.text[start:time['start']].lower():
            return time['text']
        return None

    def simple_key_value_class_recognizer(self, line):
        sim_rates = []
        ls = self.personal_information

        for phrase in ls:
            rate = fuzz.partial_ratio(phrase.lower(), line.lower())
            sim_rates.append(rate)
        index = sim_rates.index(max(sim_rates))
        if sim_rates[index]>=90 and len(ls[index])+5<len(line):
            return True
        return False


    def simple_parser(self):
        """
        label lines; the classes are three:
            simple (these are short lines, suspicious from being headlines)
            key_value (e.g. email: example@something.domain)
            complex (others)
        """
        
        for line in self.lines:
            ls = line.split(" ")
            can_be_simple = True
            try:
                pos = list(nlp(ls[-1]))[0].pos_            
                can_be_simple = pos not in ['ADP', 'AUX', 'CONJ', 'CCONJ', 'DET']
                if 'mechanics' in line.lower():

                    print("\n\n ?\n?\n {}  {} {} \n\n\n".format(pos, ls, can_be_simple))
            except:
                pass

            bcls = self.simple_key_value_class_recognizer(line)
            if (len(ls)<4 or fuzz.partial_ratio('working experience ', line.lower())>93 or \
                ('technic' in line.lower() and 'skill' in line.lower() and len(ls)<6)) and can_be_simple:
                if not bcls and ':' not in line and "-" not in line:
                    self.basic_labels.append(TypeOfLine.Simple)
                    continue

            if fuzz.partial_ratio('language', line.lower())>92 or \
                self.compute_entity_ratio('interests', line.lower())[1]>92:
                self.basic_labels.append(TypeOfLine.LongSimple)
                continue

            if (':' in line and line.index(":")< len(line)-1 and line.count(",")<2 and line.count(".")<2) or bcls:
                key = line.split(":")[0]
                # times = self.sutime.parse(key) #check whether the 'key' is not a date
                times = sutime_parser(key)
                if len(times)==0:
                    self.basic_labels.append(TypeOfLine.KeyValue)
                    continue
            if (len(ls)<4 or fuzz.partial_ratio('working experience ', line.lower())>93) and can_be_simple: #simple class again - e.g. "Language:\n" cannot pass the first-first if but nor can it pass "key-value if", but it is not complex
                self.basic_labels.append(TypeOfLine.Simple)
                continue
            
            self.basic_labels.append(TypeOfLine.Complex)

    def clean_text(self):
        """
        Heuristic rules
            Multiple continuous blanks -> Trim
            Value pair                 -> Trim
            Begin with date pair       -> Split
            Begin with part of date    -> Merge
            Begin with block key words -> Split
            Begin with comma           -> Merge
            Short text ends with comma -> Merge

        Merge means this line should be merged with the next line.
        Split means this line should be split into two lines.
        Trim means the blanks in this line should be removed.
        """

        new_lines = []
        cont = False
        for i in range(len(self.lines)):
            self.lines[i] = delete_weird_chars(self.lines[i])
            if cont:
                cont = False
                continue

            line = " ".join(self.lines[i].split()) # delete continous spaces/replace by 1 space
            if len(line) == 0 or 'background image' in line:
                continue

            times = sutime_parser(line)
            times = [item for item in times if item['type']=='DATE']
            splits = []
            start_split = 0
            # if len(times)>1: #todoi - correct some bug   print(nlp_text)
            #     for k in range(len(times)-1):
            #         if times[k]['end'] - times[k+1]['start'] <7:
            #             splits.append(times[k+1]['start'])
            if line[0] == ',' and i!=0:
                # merge backward
                line = self.lines[i-1] + line
                start_split = len(self.lines[i-1])
            if line[-1] == ',' and i!=len(self.lines)-1:
                # merge forward
                line = line + self.lines[i+1]
                cont = True
            # if i!=len(self.lines)-1 and not cont: # merge splitted dates - asi to poacha vice skody nez uzitku
            #     is_number_at_end = False
            #     is_number_at_beginning = False
            #     for m in range(len(line)-2, len(line)):
            #          if line[m].isnumeric():
            #              is_number_at_end = True
            #              break
            #     for m in range(min(2, len(self.lines[i+1])-1)):
            #         if self.lines[i+1][m].isnumeric():
            #             is_number_at_beginning = True
            #             break
                
            #     if is_number_at_beginning and is_number_at_end:
            #         # forward merge
            #         line = line + self.lines[i+1]
            #         cont = True
            
            for m, split in enumerate(splits):
                if m==0:
                    line_split = line[0+start_split:split+start_split]
                else:
                    line_split = line[splits[m-1]+start_split: split+start_split]
                new_lines.append(line_split)
            if len(splits) == 0:
                new_lines.append(line)

        self.lines = new_lines
        self.text = " | ".join(new_lines)


    def find_edu_V_experience(self):
        ratios = []
        entities_of_interest = ['experience', 'education', 'skill']
        for i, line in enumerate(self.lines):
            ls = []
            for entity in entities_of_interest:
                ent, ratio = self.compute_entity_ratio(entity, line.lower())
                ls.append(ratio)

            index = ls.index(max(ls))
            if ls[index]<90:
                continue
            ratios.append((ls[index], i))
        if len(ratios) == 0:
            return None
        ratios.sort(reverse=False, key=lambda x: x[1])
        return ratios[0]
        

    def get_personal_information(self, input_text=None, given_mail=None):
        # answer = self.model.predict(document, question)
        if input_text is None:
            mail = self.getEmail(self.text)
            if len(mail)>0:
                mail = mail[0]
                if len(mail)<5:
                    mail = None
            else:
                mail = None
            self.mail = mail
            
            birth = self.get_date_of_birth()
            
            idx = self.find_edu_V_experience()[1]

            lines = self.lines[0:idx]
            text = " | ".join(lines)
            text_ = " ".join(lines)
            phone = self.getPhone(" ".join(lines))
            name = extract_name(" ".join(lines), mail=mail)
            if mail is not None:
                text = text.replace(mail[0], "").replace("e-mail","").replace("email","").replace("mail","").replace("\n", " ")

            self.personal_info_end = idx

        questions = [
            'what is his nationality?', 'what is his web page?', 'what is his marital status?', 'what is his address?', 'what is his twitter?'
        ]
        tags = ['nationality', 'web', 'marital_status', 'address', 'twitter']
        aq = {}

        if input_text is not None:
            text_= input_text
            text = input_text
            birth = None
            phone = [None]
            name = extract_name(input_text, mail=given_mail)
            mail = given_mail
        
        for idx, q in enumerate(questions):
            if tags[idx] in text.lower():
                index = text.lower().find(tags[idx])
                l = len(text) - index
                l = min(index+l, index+84)
                text2 = text[index: l]
                if len(text2)<3:
                    text2 = text
            else:
                text2 = text
            # if tags[idx] == 'address':
            #     text2 = text_
            if len(text2)==0:
                answer = ""
            else:
                answer = self.model.predict(text2, q)['answer']
            if tags[idx] == 'web' and (len(answer)<4 or '.' not in answer[len(answer)-4:] or '@' in answer or len(answer)<7):
                answer = ""
            if len(answer) == 0:
                answer = None
            if tags[idx] == 'address' and (not has_numbers(text) or (given_mail is not None and 'address' not in text.lower())):
                answer = None
            if answer is not None and \
                (tags[idx] == 'marital_status' or tags[idx] == 'gender' or tags[idx]=='nationality' and has_numbers(answer)):
                answer = None
            aq[tags[idx]] = answer
        if len(phone)>0:
            if 'phone' in text.lower():
                index = text.find('phone')
                pp = None
                d = 1000
                for p in phone:
                    dist = abs(text.find(p)-index)
                    if dist<d:
                        d = dist
                        pp = p
                if pp is not None:
                    phone[0] = pp
            aq['phone'] = phone[0]
        else:
            aq['phone'] = None
        aq['name'] = name
        aq['birth'] = birth
        aq['mail'] = mail

        times = sutime_parser(text)
        times = list(filter(lambda x: True if '-' in x['value'] else False, times))
        if aq['birth'] is None and len(times)>0:
            aq['birth'] = times[0]['text']
        # gender
        if 'male' in text.lower():
            aq['gender'] = 'male'
        elif 'female' in text.lower():
            aq['gender'] = 'female'
        else:
            aq['gender'] = None
        for k in aq.keys():
            if k=='nationality':
                continue
            if aq['nationality'] is not None and aq[k] is not None and fuzz.partial_ratio(aq['nationality'].lower(), aq[k].lower())>91:
                aq['nationality'] = None
                break

        con_lines = " ||| ".join(self.lines)
        for k in aq.keys():
            if aq[k] is not None:
                con_lines = con_lines.replace(aq[k],"")
                self.text = self.text.replace(aq[k],"")
        self.lines = con_lines.split(" ||| ")
         
        return aq


    def get_all_non_personal_sections(self):
        beginning = self.personal_info_end
        ret_list = []
        found_entities = []


        for i in range(beginning, len(self.lines)):
            if self.basic_labels[i] == TypeOfLine.Complex or ('.' in self.lines[i] and 'language' not in self.lines[i].lower()): # ta druha podminka je s otaznikem, je to kvuli CV 5.pdf
                if self.compute_entity_ratio('interests', self.lines[i].lower())[1]<93:
                    continue
            #handle key_value:
            if self.basic_labels[i] == TypeOfLine.KeyValue and ':' not in self.lines[i][len(self.lines[i])-3:]:
                ret_list.append([self.lines[i], i, None, self.basic_labels[i]])
            else:
                # simple line
                scores = []
                for item in self.entities_of_interest:
                    if item in found_entities:
                        scores.append(0)
                        continue
                    entity, score = self.compute_entity_ratio(item, self.lines[i].lower())

                    if len(self.lines[i].lower())<3:
                        score -= 10

                    scores.append(score)

                    print("{} {} {}".format(self.lines[i], item, score))

                scores = torch.tensor(scores)
                index = torch.argmax(scores).item()

                if scores[index]<93:
                    continue

                # if self.basic_labels[i] != TypeOfLine.LongSimple:
                    # found_entities.append(self.entities_of_interest[index])
                if i+10<len(self.lines):
                    txt = " ".join(self.lines[i:i+10]).replace("/", "-")
                    times = sutime_parser(txt)
                    if self.entities_of_interest[index] in self.strictly_non_date and len(times)>1 and self.entities_of_interest[index]!='skill':
                        print("{} {}".format(self.lines[i], times))
                        continue
                
                if len(ret_list)>0 and \
                    (self.entities_of_interest[index]=='education' or self.entities_of_interest[index]=='experience')\
                         and i-ret_list[-1][1]<2 and ret_list[-1][-1] != TypeOfLine.KeyValue: #handle too short sections - which is suspicious and probably wrong
                    print("{} {}".format(ret_list, i))
                    continue 
                if self.basic_labels[i] == TypeOfLine.LongSimple:
                    ret_list.append([self.lines[i], i-1, self.entities_of_interest[index], self.basic_labels[i], 1])
                else:
                    ret_list.append([self.lines[i], i, self.entities_of_interest[index], self.basic_labels[i], 0])
        ret_list.sort(key=lambda x: x[1])
        return ret_list
            
    def process_sections(self, sections):
        if len(sections) == 0:
            return None
        section = sections[0]
        if len(sections)>1:
            end_section = sections[1][1]+sections[1][-1]
        else:
            end_section = len(self.lines)

        section_lines = self.lines[section[1]+1:end_section-1]
        sec_txt = " | ".join(section_lines)
        tms = sutime_parser(sec_txt)

        if section[2] == 'experience' or section[2] == 'education' or section[2] == 'publication' or len(tms)>3:
            # one date is one line
            new_txt = ""
            stats = [0,0]
            left = True
            right = True
            is_time_at_front = False
            cont = False
            for i in range(end_section-section[1]-1):
                if cont:
                    cont = False
                    continue
                line = self.lines[i+1+section[1]]
                line = line.replace(" - ", " - ").replace(" – ", " - ")
                counter = 0
                times = sutime_parser(line)
                # print(line)
                
                for time in times:
                    if (time['start']<5 and left) or (time['end']>len(line)-5 and right):
                        if sum(stats)>3:
                            if stats[0]>stats[1]:
                                right = False
                                left = True
                            else:
                                left = False
                                right = True

                        if time['start']<5:
                            stats[0] +=1
                        else:
                            stats[1] +=1
                    counter+=1 # nebo to vratit zpet do ifu?

                if len(new_txt)==0 and len(times)>0:
                    is_time_at_front = True
                if len(times)>0 and i!=0 and counter>0 and is_time_at_front:
                    new_txt += " || "
                
                new_txt += " | " + line
                # print(times)
                # print(new_txt)
                # print(is_time_at_front)
                # print(counter)
                # print("\n\n")

                if len(times)>0 and i!=0 and counter>0 and not is_time_at_front:
                    next_line = self.lines[i+2+section[1]].lower()
                    if i<end_section-1 and 'designation' in next_line or 'job' in next_line or 'position' in next_line or 'description' in next_line:
                        new_txt += " | " + next_line
                        cont=True
                    new_txt += " || "
                
            ls = new_txt.split(" || ")
            ls = list(map(lambda x: x.strip(), ls))
        else:
            if section[2] is None:
                ls = []
            else:
                new_txt = ""
                for i in range(end_section-section[1]-1):
                    line = self.lines[i+1+section[1]]
                    line = line.replace(" - ", " - ").replace(" – ", " - ")
                    new_txt += " | " + line
                ls = new_txt.split(" || ")
                ls = list(map(lambda x: x.strip(), ls))
            
        next_ret = self.process_sections(sections[1:])
        if next_ret is None:
            return [ls]
        else:
            next_ret.reverse()
        if len(ls)==0:
            ls = [next_ret]
        else:
            next_ret.extend([ls])
            next_ret.reverse()  
        return next_ret

    def parse_information_from_date_based_section(self, sections, processed_sections, section_name, line_n):
        """
        parses particular items based on dates from a given section and moreover parses
        each item; e.g. education section: parses particular items based on dates and
        for each item determines what did he/she study and at what institution
        """
        processed_section = None
        for idx, section in enumerate(sections):
            if section[1]==line_n:
                processed_section = processed_sections[idx]
                break
        if processed_section is None:
            return None, None
        ls = []
        check_activated = False
        is_duration = False
        # print("{} \n {}".format(section_name, processed_section))

        times = sutime_parser(" ".join(processed_section))
        if len(times)==0:
            print(processed_section)
            print("The section: {} will be considered as non date based".format(section_name))
            return self.parse_information_from_section(sections, processed_sections, section_name, line_n)

        for idx, line in enumerate(processed_section):
            if len(line)==0:
                continue
            if len(line)>650:
                check_activated = True

            times = sutime_parser(line, no_last_time=True)
            if is_duration:
                    times = filter_duration_dates(times)
            # if section_name == 'education':
            #     print(line)
            #     print(times)
            #     print("\n")
            if len(times)==0:
                line = line.replace("/", "-")
                times = sutime_parser(line)
                if is_duration:
                    times = filter_duration_dates(times)

            if len(times)==0:
                continue
            time = times[0]
            if idx==0 and check_activated:
                if isinstance(time['value'], dict):
                    is_duration = True
            p_line = line.replace(time['text'], "")
            p_line2 = p_line.replace(" | ", " ")
            if ('year' in time['text'] or 'old' in time['text']) and not has_numbers(time['text']):
                continue
            parsed_line = {}
            if section_name in self.specific_questions.keys():
                for idx2, question in enumerate(self.specific_questions[section_name]):

                    if len(p_line2.replace(" ", "").replace("\n", "").strip())<2 or len(p_line.replace(" ", "").replace("\n", "").strip())<2:
                        parsed_line[self.spec_tags[section_name][idx2]]=None
                        continue
                    if 'faculty' in question and not 'faculty' in p_line:
                        continue
                    answer = self.model.predict(p_line, question, hia=True)['answer']
                    answer = answer.replace(" | ", " ")
                    answer2 = self.model.predict(p_line2, question, hia=True)['answer']
                    ans_backup = answer
                    if len(answer2)>len(answer) and fuzz.partial_ratio(answer, answer2)>90:
                        answer = answer2
                    if len(answer)==0:
                        answer = None
                    p_line = p_line.replace(ans_backup, "")
                    p_line2 = p_line2.replace(answer2, "")

                    parsed_line[self.spec_tags[section_name][idx2]]=delete_weird_chars(answer)
            else:
                parsed_line = p_line.replace("|", ";").replace(";  ; ", "").replace("| : | ", "")

            cc = False
            if isinstance(parsed_line, dict) and None in parsed_line.values():
                for k in parsed_line.keys():
                    parsed_line[k] = delete_weird_chars(line.replace(time['text'], "").replace("|  | ",""))
                    if len(parsed_line[k])<14:
                        cc = True
                        break
            if cc:
                continue

            ls.append((time['text'], parsed_line))
            # block  = p_line
        # filter out bad items with nonsense keys:
        # ls2 = []
        # for item in ls:
        #     try:
        #         if isinstance(item[0], dict):
        #             parse(item[0]['begin'])
        #             ls2.append(item)
        #         else:
        #             parse(item[0])
        #             ls2.append(item)
        #     except:
        #         pass
        # ls = ls2
        # ls.sort(key = lambda x: parse(x[0]) if not isinstance(x[0], dict) else parse(x[0]['begin']))
        dictic = {}
        for item in ls:
            key = None
            if isinstance(item[0], dict):
                key = item[0]['begin'] 
            else:
                key = item[0]
            while key in dictic:
                key += " x"
            dictic[key] = item[1]
            
        return dictic, ls


    def parse_information_from_section(self, sections, processed_sections, section_name, line_n):
        """
        parses arbitrary section (hopefully) - can be used for date-based sections too, but
        this method has lower accuracy than the method above
        """

        def doit(processed_section, recursion_allowed=True):
            ls = []
            for line_tmp in processed_section:
                line = delete_weird_chars(line_tmp)
                if len(line)<11 and self.contains_fw(line):
                    continue
                if len(line)<3:
                    continue
                if 'declare' in line.lower() and recursion_allowed:
                    l = line.split("|")
                    if len(l)==0: continue
                    if len(l[0])>3 or len(l)==1:
                        txt = l[0]
                    else:
                        txt = l[1]
                    return doit([txt], recursion_allowed=False)

                for item in self.forbidden_words_in_section:
                    if item in line.lower():
                        break
                s = local_parser(line)
                lss = s.split("|")
                for item in lss:
                    tmp = item.split(" ")
                    if len(tmp)>4 and ':' not in line and '-' not in line and section_name not in self.long_sec_items:
                        s = local_parser2(line)
                        lss = s.split("|")
                        break
                lss = list(map(lambda x: x.strip(), lss))
                lss = list(filter(lambda x: True if len(x)>0 else False, lss))
                ls.extend(lss)
            return ls
        ls = []
        local_parser = lambda x: ''.join(list(map(lambda k: k if k.isalnum() or k==" " or k=="\n" or k=="'" or k==":" or k=='.' or k=='-' else '|', x)))
        local_parser2 = lambda x: ''.join(list(map(lambda k: k if k!="." else '|', x)))
        for idx, section in enumerate(sections):
            if section[1]==line_n:
                processed_section = processed_sections[idx]
                if section_name == 'personal':
                    text = " ".join(processed_section).replace(" | ", ' ')
                    ret = self.get_personal_information(text, given_mail=self.mail)
                    return None, [ret]
                if fuzz.partial_ratio('language', section[0].lower())>=90:
                    ret = self.extract_language_skills(processed_section, local_parser)
                else:
                    for lls in processed_section[0].split(" | "):
                        try:
                            pos = list(nlp(lls.split(" ")[-1]))[0].pos_
                        except:
                            continue
                        if pos in ['ADP', 'AUX', 'CONJ', 'CCONJ', 'DET']:
                            processed_section = [" ".join(processed_section[0].split("|"))]
                            break

                    times = sutime_parser(" | ".join(processed_section).replace("/", "-"))
                    
                    if len(times)>3 and section_name not in self.strictly_non_date:
                        return self.parse_information_from_date_based_section(sections, processed_sections, section_name, line_n)
                    ret = doit(processed_section)
                    ret2 = []
                    for item in ret:
                        if ':' in item:
                            ret2.append(" ".join(item.split(":")[1:]))
                        else:
                            ret2.append(item)
                    ret = ret2
                ret = list(filter(lambda x: True if len(x)>0 else False, ret))
                if section_name == 'projects' or section_name=='education' or section_name=='publication' or section_name=='experience':
                    ret = [" ".join(ret)]
                ls.extend(ret)
        return ls, ls

    def extract_language_skills(self, processed_section, local_parser):
        languages = []

        for line in processed_section:
            # print("debug {}".format(line))
            s = local_parser(line)
            lss = s.split("|")
            for item in lss:
                if len(item)<3:
                    continue
                for lang in self.langs:
                    if fuzz.partial_ratio(lang, item.strip().lower())>95 and len(lang)>3 or fuzz.partial_ratio(lang, item.strip())>99:
                        indices = []
                        for level in self.lang_levels:
                            idx = line.find(level)
                            if idx!=-1:
                                indices.append((idx, level))

                        idx = line.find(item)
                        indices.sort(key=lambda x: abs(idx-x[0]+4))
                        if len(indices)>0:
                            level = indices[0][1]
                        else:
                            level = None
                        languages.append((lang, level))
                        continue
        return languages

    def filter_nodate_date_sections(self, sections):
        new_ls = []
        for idx, section in enumerate(sections):
            if idx < len(sections)-1:
                end_idx = sections[idx+1][1]
            else:
                end_idx = len(self.lines)-1
            if section[2] in self.date_based_entities:
                new_ls.append(section)
            else:
                block = " | ".join(self.lines[section[1]: end_idx])
                # times = self.sutime.parse(block)
                times = sutime_parser(block)
                times = list(filter(lambda x: True if len(x['text'])>4 else False, times))
                if len(times) == 0:
                    new_ls.append(section)
        return new_ls

    def extract_KV_info(self):
        ls = []
        local_parser = lambda x: ''.join(list(map(lambda k: k if k.isalnum() or k==" " or k=="\n" or k=="'" or k=='.' else '|', x)))
        for idx in range(len(self.lines)):
            if self.basic_labels[idx] == TypeOfLine.KeyValue:
                # print(self.lines[idx])
                p_line = local_parser(self.lines[idx])
                p_line = p_line.split("|")
                ls.append({p_line[0]: p_line[1:]})
        return ls


    def parse_additional_info(self, additional_info):
        # TODO.. extract important information from the dictionary
        # language should be ok to be parsed
        #..
        pass

    def parse_info_from_personal_section(self, personal_section, parsed_personal_info):
        personal_section = personal_section[0]
        for key in personal_section.keys():
            if personal_section[key] is not None and parsed_personal_info[key] is None:
                if 'birth' == key or 'gender' == key or 'marital_status' == key or 'nationality' == key:
                    parsed_personal_info[key] = personal_section[key]
                if 'name' == key:
                    name_text = self.text[0:200].replace("\n", " ").replace("|", "").replace("  "," ")
                    ls = personal_section[key].split(" ")
                    if len(ls) == 2 and ls[0][0].isupper() and \
                        ls[1][0].isupper() and len(ls[0])>3 and len(ls[1])>3 and \
                        personal_section[key].lower() in name_text:  

                        parsed_personal_info[key] = personal_section[key]
            elif personal_section[key] is None and parsed_personal_info[key] is not None:
                if key =='nationality' or key=='gender' or key=='marital_status':
                    parsed_personal_info[key]=None
        return parsed_personal_info          


    def main(self):
        self.clean_text()
        self.simple_parser()
        for i in range(len(self.lines)):
            print("{}  {}  {}".format(self.lines[i], self.basic_labels[i], i))
        ls = self.find_edu_V_experience()
        personal_info = self.get_personal_information()
        ls_sections = self.get_all_non_personal_sections()
        # ls_sections = self.filter_nodate_date_sections(ls_sections) - tohle je treba jeste promyslet
        ls_main_sections = list(filter(lambda x: False if x[2] is None else True, ls_sections))
        ls = self.process_sections(ls_main_sections)
        print(ls_main_sections)
        # print(ls)
        # experience,_ = self.parse_information_from_date_based_section(ls_main_sections, ls, 'experience')
        # education,_ = self.parse_information_from_date_based_section(ls_main_sections, ls, 'education')
        # publication,_ = self.parse_information_from_date_based_section(ls_main_sections, ls, 'publication')
        dictic = {}

        for sec in ls_main_sections:
            if sec[2] not in self.date_based_entities:
                d, l = self.parse_information_from_section(ls_main_sections, ls, sec[2], sec[1])
                # print(d)
                if d is None:
                    if sec[2] in dictic:
                        dictic[sec[2]].extend(l)
                    else:
                        dictic[sec[2]]=l
                else:
                    if isinstance(d, dict):
                        dictic.update({sec[2]:d})
                    elif sec[2] in dictic.keys() and isinstance(dictic[sec[2]], dict):
                        pass
                    elif sec[2] in dictic.keys():
                        dictic[sec[2]].extend(d)
                    else:
                        dictic[sec[2]] = l

            else:
                d, l = self.parse_information_from_date_based_section(ls_main_sections, ls, sec[2], sec[1])
                # print("date based section: {}  ret {}".format(sec[2], d))
                if isinstance(d, dict):
                    if sec[2] in dictic.keys() and isinstance(dictic[sec[2]], dict):
                        dictic[sec[2]].update(d)
                    else:
                        dictic[sec[2]] = d
                            
                else:
                    if sec[2] in dictic.keys() and not isinstance(dictic[sec[2]], dict):
                        dictic[sec[2]].extend(d)
                    else:
                        dictic[sec[2]] = d

        # skills = self.parse_information_from_section(ls_main_sections, ls, 'skill')
        additional_info = self.extract_KV_info()

        if 'personal' in dictic.keys():
            personal_info = self.parse_info_from_personal_section(dictic['personal'], personal_info)
            del dictic['personal']
        # ret = {'personal_info': personal_info, 'experience': experience, 'education': education, 'publication': publication,
        #         } # perhaps add additional info in the future?
        ret = {'personal_info': personal_info}
        ret.update(dictic)

        return ret


if __name__ == "__main__":
    information_extractor = InformationExtractor('resumes/30.txt')
    # information_extractor.filter_out_empty_linedicts()

    # information_extractor.clean_text()
    ret = information_extractor.main()
    for k in ret.keys():
        print('{}:\n{}'.format(k,ret[k]))
        print("\n\n")